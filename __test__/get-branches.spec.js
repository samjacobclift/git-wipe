import test from 'ava'

import {stripGitBranch} from '../src/get-branches'

test('striping git branches should remove *', t => {
  const returnListBranches = stripGitBranch('* master\ndev\n * release')
  t.deepEqual(returnListBranches, ['master', 'dev', 'release'])
})
