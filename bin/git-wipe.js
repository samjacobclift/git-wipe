#!/usr/bin/env node

const {deleteBranch} = require('../src/delete-branch')
const {getBranchNames} = require('../src/get-branches')
const {inRepo} = require('../src/in-repo')

const yargs = require('yargs')
const args = yargs
  .array('branches')
  .alias('b', 'branches')
  .boolean('fake')
  .alias('f', 'fake')
  .boolean('remote')
  .alias('r', 'remote')
  .argv

const branchesToKeep = args.branches
const deleteRemote = args.remote || false
const fakeRun = args.fake || false

if (fakeRun) {
  console.log('fake run')
}

// check we are in a repo
// before trying to delete branches
inRepo()
  .then(() => {
    // Get all the branch names
    getBranchNames()
      .then(branchNames => {
        const branchesToDelete = branchNames
          .filter(branch =>
            !branchesToKeep.find(branchToKeep => branchToKeep === branch))
        // delete each branch
        branchesToDelete.forEach((branch) => {
          deleteBranch(branch, deleteRemote, fakeRun)
        })
      })
      .catch((err) => {
        console.log('Error listening and deleting branches ', err)
      })
  })
  .catch(() => {
    console.log('Not inside a git repo ')
  })
