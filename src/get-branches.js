const util = require('util')
const exec = util.promisify(require('child_process').exec)

async function getBranchNames () {
  const { stdout, stderr } = await exec('git branch')
  if (stderr) {
    console.log('stderr:', stderr)
  }

  return stripGitBranch(stdout)
}

const stripGitBranch = (gitBranchResult) => {
  let branches = gitBranchResult.split('\n')
  return branches
    .filter(branch => branch)
    .map(branch => {
      if (branch.includes('*')) {
        return branch.split('*')[1].trim()
      }
      return branch.trim()
    })
}

module.exports = {
  getBranchNames,
  stripGitBranch
}
