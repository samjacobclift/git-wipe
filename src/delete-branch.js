const util = require('util')
const exec = util.promisify(require('child_process').exec)

async function deleteBranch (branchToDelete, deleteRemote, fakeRun) {
  if (!fakeRun) {
    console.log('deleting local branch ', branchToDelete)
    const { stderr } = await exec('git branch -D ' + branchToDelete)
    if (stderr) {
      console.log('stderr:', stderr)
    }
    if (deleteRemote) {
      console.log('deleting remote branch ', branchToDelete)
      await exec('git push origin --delete ' + branchToDelete)
    }
  }
}

module.exports = {
  deleteBranch
}
