const util = require('util')
const exec = util.promisify(require('child_process').exec)

async function inRepo () {
  const { stderr } = await exec('git rev-parse --is-inside-work-tree')
  if (stderr) {
    console.error(`exec error: ${stderr}`)
    return false
  }
  return true
}

module.exports = {
  inRepo
}
