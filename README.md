# Git-wipe

Command line tool to delete branches you have locally and then delete those branches on origin

## Install
```
npm install -g git-wipe
```

## Usage
```
git-wipe --branches master release/q2-sprint-1 release/q2-sprint-2 --fake true
```